# memogarcia.mx

[![pipeline status](https://gitlab.com/memogarcia/memogarcia.mx/badges/master/pipeline.svg)](https://gitlab.com/memogarcia/memogarcia.mx/commits/master)

Personal blog

## Building new versions with Hugo

    hugo

Run a local server

    hugo server

## References

* [nginx static files](https://docs.nginx.com/nginx/admin-guide/web-server/serving-static-content/)
* [Renewing let's encrypt TLS certificates with Certbot](https://letsencrypt.readthedocs.io/en/latest/using.html#renewal)
