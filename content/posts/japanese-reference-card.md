---
title: "日本 参照カード"
date: 2019-11-22T07:53:23+01:00
draft: false
tags: japanese
---

## ひらがな + カタカナ

|   | a         | i         | u         | e        | o        |
|---|-----------|-----------|-----------|----------|----------|
| - | a あ ア   | i い イ   | u う ウ   | e え エ  | o お オ  |
| k | ka か カ  | ki き キ  | ku く ク  | ke け ケ | ko こ コ |
| s | a さ サ   | shi し シ | su す ス  | se せ セ | so そ ソ |
| t | ta た タ  | chi ち チ | tsu つ ツ | te て テ | to と ト |
| n | na な ナ  | ni に ニ  | nu ぬ ヌ  | ne ね ネ | no の ノ |
| h | ha は ハ  | hi ひ ヒ  | fu ふ フ  | he へ ヘ | ho ほ ホ |
| m | ma ま マ  | mi み ミ  | mu む ム  | me め メ | mo も モ |
| y | ya や ヤ  |          | yu ゆ ユ  |          | yo よ ヨ |
| r | ra ら ラ  | ri り リ  | ru る ル  | re れ レ | ro ろ ロ |
| w | wa わ ワ  |           |           |          | wo を ヲ |
| - | n ん ン |           |           |          |          |
| g | ga が ガ  | gi ぎ ギ  | gu ぐ グ  | ge げ ゲ | go ご ゴ |
| z | za ざ ザ  | ji じ ジ  | zu ず ズ  | ze ぜ ゼ | zo ぞ ゾ |
| d | da だ ダ  | ji ぢ ヂ  | zu づ ヅ  | de で デ | do ど ド |
| b | ba ば バ  | bi び ビ  | bu ぶ ブ  | be べ ベ | bo ぼ ボ |
| p | pa ぱ パ  | pi ぴ パ  | pu ぷ プ  | pe ぺ ペ | po ぽ ポ |
