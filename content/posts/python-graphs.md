---
title: "Graph data structures in Python"
date: 2019-01-11T19:06:46+01:00
draft: true
tags: python, graphs, functional programming
---

A graph is a matematical model that is useful to map relations between objects.

In a more technical way: A graph has noded (vertices) that can be connected to other by an edge.

types:

* undirected graph
* directed graph

{{< highlight python "linenos=table" >}}
def hello():
    return
{{< / highlight >}}

## References

* <https://www.python-course.eu/graphs_python.php>
* <http://networkx.github.io/>